# Release Notes

### Fixed
Fix import issue to powershell gallery.


### Fixed
Fixed issue when getting system locale code in the `New-PSPackage` function.

---

PSPackage includes a variety of changes. Please consult the diff to see what's new.