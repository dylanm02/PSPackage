# PSPackage
![GitLab Release (latest by date)](https://img.shields.io/gitlab/v/release/34849946) ![PowerShell Gallery Version](https://img.shields.io/powershellgallery/v/PSPackage) 

A simple powershell module that creates the required structure needed for creating your own powershell module. The PSPackage module also has an addational helper command that creates functions for your powershell module.

## Documentation
Documentation on the PSPackage module can be found on our [Wiki pages](https://gitlab.com/dylanm02/PSPackage/-/wikis/home).

## Installation
### From the PowerShell Gallery

Installing items from the Gallery requires the latest version of the PowerShellGet module, which is available in Windows 10, in Windows Management Framework (WMF) 5.0, or in the MSI-based installer (for PowerShell 3 and 4).

Open Powershell and run the following command:

```PowerShell tab=
PS> Install-Module -Name PSPackage
```

### From the GitHub release page

_**This is usually the same as the Powershell Gallery version**_

1. [Click here](https://gitlab.com/dylanm02/PSPackage/-/releases) to go to the latest releases, then download the *PSPackage.zip* file attached to the release
2. Right-click the downloaded zip, select Properties, then unblock the file.
    _This is to prevent having to unblock each file individually after unzipping._
3. Unzip the archive.
4. (Optional) Place the module somewhere in your PSModulePath.
    * You can view the paths listed by running the environment variable `$env:PSModulePath`

## Support
Need support? No problem, just [raise an issue](https://gitlab.com/dylanm02/PSPackage/-/issues/new). When creating a support issue, please add as much information as possible (code snippets, error messages, etc) and remember to apply the 'support' tag to your issue.

## Roadmap
We currently have a roadmap that is freely available on our [milestones page](https://gitlab.com/dylanm02/PSPackage/-/milestones).

## Contributing
We are currently not open to code contributions however we are open to suggestions and feature requests. If you would like to see any particular features or have any suggestions, please feel free to [raise an issue](https://gitlab.com/dylanm02/PSPackage/-/issues/new) with the 'suggestion' tag. 

## Authors and acknowledgment
Special thanks to Warren Frame. Warren created a blog post on how to create powershell modules and has set a good standard on how to do so. Go and check Warren out [here](http://ramblingcookiemonster.github.io/). 

## License
This project is licensed under the GNU General Public License v3.0. For more information on the GNU General Public License v3.0, please read the [LICENSE.md](https://gitlab.com/dylanm02/PSPackage/-/blob/main/LICENSE) file.

## Project status
This project is in active development and new features are added as and when they are needed. If you would like to see any particular features or have any suggestions, please feel free to [raise an issue](https://gitlab.com/dylanm02/PSPackage/-/issues/new) with the 'suggestion' tag.
