<#
.SYNOPSIS
Creates a new powershell module structure

.DESCRIPTION
This function will create the required structure needed for your own powershell module.

.PARAMETER RootPath
The path of the where you would like to save your module. This is required.

.PARAMETER ModuleName
The name of the module that you would like to create. This is required.

.PARAMETER Author
The name of the module's author. This is required.

.PARAMETER Description
The description what the module will do. This is not required.

.INPUTS
None. You cannot pipe objects to Add-Extension.

.OUTPUTS
None unless you add the -Verbose parmeter.

.EXAMPLE
PS> New-PSPackage -RootPath 'C:\Users\admin\Powershell Modules' -ModuleName 'ADLink' -Author 'Dylan M'

.EXAMPLE
PS> New-PSPackage -RootPath 'C:\Users\admin\Powershell Modules' -ModuleName 'ADLink' -Author 'Dylan M' -Description "This is a cool module that links to AD!" -Verbose
The ADLink module has been successfuly created.
#>

function New-PSPackage
{
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory=$true)]
        [string] $RootPath,
        [string] $ModuleName,
        [string] $Author,
        [Parameter(Mandatory=$false)]
        [string] $Description = ""
    )

    Begin
    {

        $SystemLocaleCode = Get-SystemLocaleCode
        
        $PSM1Content = Get-Content -Raw -Path "$($PSModuleRoot)\bin\psm1.content"
        $TemplateFunction = Get-Content -Raw -Path "$($PSModuleRoot)\bin\template-function.ps1.content"

        if (Test-Path "$($RootPath)\$($ModuleName)") {

            Write-Error "Unable to create the '$($ModuleName)' module as the directory is dirty. Please delete the '$($ModuleName)' folder from '$($RootPath)' or specify a different path."
            break;

        }
        
    }

    Process
    {
        # Create Root Directory
        [void](New-Item -Path $RootPath -Name $ModuleName -ItemType "directory")
        [void](New-Item -Path "$($RootPath)\$($ModuleName)" -Name $ModuleName -ItemType "directory")

        # Create Sub Directory's
        [void](New-Item -Path "$($RootPath)\$($ModuleName)" -Name "Tests" -ItemType "directory")
        [void](New-Item -Path "$($RootPath)\$($ModuleName)\$($ModuleName)" -Name $SystemLocaleCode -ItemType "directory")
        [void](New-Item -Path "$($RootPath)\$($ModuleName)\$($ModuleName)" -Name "bin" -ItemType "directory")
        [void](New-Item -Path "$($RootPath)\$($ModuleName)\$($ModuleName)" -Name "lib" -ItemType "directory")
        [void](New-Item -Path "$($RootPath)\$($ModuleName)\$($ModuleName)" -Name "Private" -ItemType "directory")
        [void](New-Item -Path "$($RootPath)\$($ModuleName)\$($ModuleName)" -Name "Public" -ItemType "directory")

        # Create Required Files
        [void](New-Item -Path "$($RootPath)\$($ModuleName)\Tests\$($ModuleName).Tests.ps1" -ItemType File)
        [void](New-Item -Path "$($RootPath)\$($ModuleName)\$($ModuleName)" -Name "$($ModuleName).Format.ps1xml" -ItemType File)
        [void](New-Item -Path "$($RootPath)\$($ModuleName)\$($ModuleName)" -Name "$($ModuleName).psd1" -ItemType File)
        [void](New-Item -Path "$($RootPath)\$($ModuleName)\$($ModuleName)" -Name "$($ModuleName).psm1" -ItemType File -Value $PSM1Content)
        [void](New-Item -Path "$($RootPath)\$($ModuleName)\$($ModuleName)\Public" -Name "Template-Function.ps1" -ItemType File -Value $TemplateFunction)
        [void](New-Item -Path "$($RootPath)\$($ModuleName)\$($ModuleName)\$($SystemLocaleCode)" -Name "about_$($ModuleName).help.txt" -ItemType File)

        # Create Manifest
        New-ModuleManifest -Path "$($RootPath)\$($ModuleName)\$($ModuleName)\$($ModuleName).psd1" `
                   -RootModule "$($ModuleName).psm1" `
                   -Description $Description `
                   -Author $Author `
                   -FormatsToProcess "$ModuleName.Format.ps1xml"

        Write-Verbose "The $($ModuleName) module has been successfuly created."
    }
}