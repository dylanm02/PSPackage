<#
.SYNOPSIS
Creates a new powershell function

.DESCRIPTION
This function will create the required file needed for a powershell module function.

.PARAMETER Name
This is the name of the function. This is required.

.PARAMETER Path
Path of the powershell module. This is not required

.PARAMETER Visibility
This set whether the function is public or private. This is required

.INPUTS
None. You cannot pipe objects to Add-Extension.

.OUTPUTS
None unless you add the -Verbose parmeter.

.EXAMPLE
PS> New-PSPackageFunction -Name Get-TemplateFiles -Visibility Private

.EXAMPLE
PS> New-PSPackageFunction -Name New-User -Visibility Public -Verbose
The New-User function has been successfuly created.
#>

function New-PSPackageFunction
{
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory=$true)]
        [string] $Name,

        [Parameter(Mandatory=$false)]
        [string] $Path,

        [Parameter(Mandatory=$false)]
        [ValidateSet("Public", "Private")]
        [string] $Visibility
    )

    Begin
    {

        # Create path variable if not exists
        if (!($Path)) {

            $Path = (Get-Location).Path

        }

        # Check if the function already exists
        if (Test-Path "$($Path)\$($Visibility)\$($Name).ps1") {

            Write-Error "Unable to create the '$($Name)' function there is already a function with that name. Please delete the '$($Name)' function or specify a different function name."
            break;

        }

        # Check if the function name is formatted correctly
        if (!($Name -match '[a-zA-Z]+-[a-zA-Z]+')) {

            Write-Error "Unable to create the '$($Name)' function as the function doesn't match formatting standards. Please specify a different function name."
            break;

        }

        # Check if the function name is an approve verb
        $ApprovedVerbs = Get-Content -Raw -Path "$($PSModuleRoot)\bin\approved-verbs.json" | ConvertFrom-Json -Depth 10
        $HasAppovedVerb = $false
        foreach ($ApprovedVerb in $ApprovedVerbs.verb) {

            if ($Name.StartsWith("$($ApprovedVerb)-")) {

                $HasAppovedVerb = $true

            }

        }

        if (!($HasAppovedVerb)) {

            Write-Error "Unable to create the '$($Name)' function as the function doesn't start with an approved verb. Please specify a different function name."
            break;

        }

        $TemplateFunction = Get-Content -Raw -Path "$($PSModuleRoot)\bin\template-function.ps1.content"

        $TemplateFunction = $TemplateFunction.Replace("{{functionName}}", $Name)
        
    }

    Process
    {
        # Create Required Files
        [void](New-Item -Path "$($Path)\$($Visibility)\$($Name).ps1" -ItemType File -Value $TemplateFunction)

        Write-Verbose "The $($Name) function has been successfuly created."
    }
}