function Get-SystemLocaleCode
{
    if ($PWSHIsWindows) {

        $SystemLocaleCode = (Get-WinSystemLocale).Name

    } elseif (($PWSHIsMacOS) -or ($PWSHIsLinux)) {

        $SystemLocaleCode = locale | ForEach-Object { if($_.StartsWith("LANG=")){$_ -replace 'LANG="', '' -replace '[^.]*$', '' -replace '[.]'}}

    }

    return $SystemLocaleCode;
}