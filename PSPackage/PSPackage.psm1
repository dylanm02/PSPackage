#Get public and private function definition files.
$Public  = @( Get-ChildItem -Path $PSScriptRoot\Public\*.ps1 -ErrorAction SilentlyContinue )
$Private = @( Get-ChildItem -Path $PSScriptRoot\Private\*.ps1 -ErrorAction SilentlyContinue )

#Dot source the files
Foreach($import in @($Public + $Private))
{
    Try
    {
        . $import.fullname
    }
    Catch
    {
        Write-Error -Message "Failed to import function $($import.fullname): $_"
    }
}

# Set variables visible to the module and its functions only
$PSModuleRoot = $PSScriptRoot

# Set variables with corresponding values based on the OS of the computer
if ($PSVersionTable.PSEdition -eq 'Desktop') {

    $PWSHIsWindows = $true;
    $PWSHIsMacOS = $false;
    $PWSHIsLinux = $false;

} else {

    $PWSHIsWindows = $IsWindows;
    $PWSHIsMacOS = $IsMacOS;
    $PWSHIsLinux = $IsLinux;

}

# Export Public functions ($Public.BaseName) for WIP modules
Export-ModuleMember -Function $Public.Basename